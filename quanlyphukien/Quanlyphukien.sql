﻿use master
go
if (exists(select * from sysdatabases where name = 'QuanLyPhuKien'))
drop database QuanLyPhuKien
go
create database QuanLyPhuKien
go 
use QuanLyPhuKien
go
create table customer
(
	id int primary key identity(1,1),
	username varchar(100),
	hashpass varchar(100),
	name nvarchar(100),
	address nvarchar(200),
	email varchar(100),
	phone nvarchar(20),
	total float default 0
)
go
create table brand
(
	id int primary key identity(1,1),
	name nvarchar(100),
	img varchar(100),
	description nvarchar(1000) 
)
go
create table category
(
	id int primary key identity(1,1),
	name nvarchar(100),
	img varchar(100),
	description nvarchar(100)
)
go

create table product
(
	id int primary key identity(1,1),
	name nvarchar(100),
	img varchar(100),
	price float,
	category int foreign key references category(id),
	description nvarchar(1000),
	monthOfGuaranty int,
	brand int foreign key references brand(id)
)
go
go
create table bill
(
	id int primary key identity(1,1),
	customer int foreign key references customer(id),
	total float default 0,
	status bit
)
go

go
create table billDetail
(
	id int primary key identity(1,1),
	product int foreign key references product(id),
	quantity int,
	billId int foreign key references Bill(id)
)
go
insert into  category (name, img, description) values
(N'Tai Nghe','tainghecat.jpg',N'tai nghe tốt'),
(N'Ốp lưng','oplungcat.jpg',N'ốp lưng tốt'),
(N'Sạc dự phòng','sacduphongcat.jpg',N'Sạc dự phòng')
delete from category

insert into  category (name, img, description) values
(N'Cáp sạc','tainghecat.jpg',N'tai nghe tốt'),
(N'Củ sạc','oplungcat.jpg',N'ốp lưng tốt'),
(N'Túi da','sacduphongcat.jpg',N'Sạc dự phòng')
insert into brand(name,	img,description ) values
('Apple','apple.png',N'Ngày nay, ngoài thiết bị điện thoại smartphone hay iPad, người dùng còn rất chú ý đến phụ kiện của chúng. Phụ kiện Apple mang lại cho người dùng những sản phẩm chất lượng và phong cách hiện đại, trẻ trung. Phụ kiện apple chính hãng giá rẻ là nhu cầu của rất nhiều người trong thời đại hiện nay, đặc biệt là các bạn trẻ. Trên thị trường hiện nay có rất nhiều sản phẩm phụ kiện apple được các đơn vị khác nhau bày bán trên thị trường.'),
('Samsung', 'samsung.jpg',N'Phụ kiện Samsung chính hãng không chỉ bền, đẹp, chất lượng cao mà còn tương thích tối đa với các thiết bị của Samsung, không ảnh hưởng đến hoạt động hay tuổi thọ của thiết bị. Gomhang.vn chuyên cung cấp phụ kiện chính hãng cho các dòng máy của Samsung với giá tốt nhất và chính sách bảo hành 6 tháng 1 đổi 1 tại nhà giúp khách hàng luôn an tâm.Giảm giá 5% cho các đơn hàng có từ 2 sản phẩm >39k'),
('Oppo','opple.jpg',N'Phụ kiện oppo giá rẻ, mẫu mã đẹp chất lượng cao bảo hành 1 năm 1 đổi 1 với nhiều khuyến mãi.'),
('Bphone','bphone.jpg',N'Mua Phụ kiện cho Điện thoại Bphone 2017 giá rẻ, chất lượng, đa dạng mẫu mã: Pin sạc dự phòng, ốp lưng, adapter sạc, cáp sạc, tai nghe, loa,...')
go
delete from brand
delete from brand where name='Bphone';
--password: admin
insert into customer(username,hashpass ,name ,address ,email, phone) values
('abc','d033e22ae348aeb5660fc2140aec35850c4da997','abc','Hanoi','abc@gmail.com', '0123456789')
go
insert into customer(username,hashpass ,name ,address ,email, phone) values
('cde','7c222fb2927d828af22f592134e8932480637c0d','cde','Hanoi','abc@gmail.com','0346853232')
go
/*create trigger addBillDetail
on  billDetail
after insert
begin
	declare @price = select top 1 price from billDetail join bill on billDetail.billId = bill.id
	join product on billDetail.product = product.id
	where billDetail.billId  = (billDetail.id)
	declare @sale
	update bill set total = total +
end
go*/
insert into product(name,img,price,description,monthOfGuaranty,brand,category) values
('Tai nghe Bluetooth Samsung MG900E', 'SamsungMG900E.jpg', 200000, N'Công nghệ chống ồn cho âm thanh trong trẻo và chất lượng. Đệm tai nghe êm ái, dễ chịu khi sử dụng thời gian dài. Thời gian thoại lên đến 9 giờ, thời gian chờ lên đến 300 giờ. Thời gian sạc khoảng 2 giờ. Thời gian nghe nhạc có thể lên đến 8 giờ.', 6 , 2, 1),
('Apple Watch HocoCW16', 'AppleWatchHocoCW16.jpg', 4500000, N'Sạc không dây tương thích Apple Wach 1/2/3/4
Dòng điện an toàn khi sử dụng
Bộ cáp sạc không dây đồng hồ Apple watch hút nam châm Hoco CW16 cao cấp chế tạo bằng vật liệu thép không gỉ, thiết kế thời trang nhỏ gọn và tiện lợi Sạc Không Dây Apple Watch Hoco CW16 tương thích với các dòng Apple Watch 1/2/3/4.', 12, 1, 5),
(N'Ốp lưng Aramid Standing cho Samsung Galaxy Z Fold 2 ','Opsamsung.jpg',1200000,N'phụ kiện bảo vệ điện thoại được sử dụng phổ biến, đặc việt với Samsung Galaxy Z Fold 2. Nếu bạn đang sở hữu mẫu điện thoại đặc biệt này nhưng chưa tìm được ốp lưng ưng ý, hãy tham khảo ốp lưng Aramid Standing cho Samsung Galaxy Z Fold 2.

Thiết kế ôm khít – cầm nắm chắc chắn
Cũng nhưng mọi chiếc ốp khác, ốp lưng Aramid Standing có thiết kế ôm khít điện thoại. Nhờ đó Galaxy Z Fold 2 có thể tránh khỏi nhứng trầy xước ở mặt lưng, góc cạnh. Thiết kế này cũng giúp hạn chế bụi bẩn bám vào máy.

op-lung-aramid-standing-cho-galaxy-z-fold-2-1',6,2,2)

delete from product where name='Tai nghe Bluetooth Samsung MG900E';
select * from product
select * from customer
