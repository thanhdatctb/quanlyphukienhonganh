﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace QuanLyPhuKien.Models
{
    public class CartItem
    {
        public product product { get; set; }
        public int quantity { get; set; }
        public double total
        {
            get
            {
                return (double) product.price * quantity;
            }
        }
        public CartItem() { }
        public CartItem(product product, int quantity)
        {
            this.product = product;
            this.quantity = quantity;
        }
    }
}