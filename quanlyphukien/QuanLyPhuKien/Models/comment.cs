//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuanLyPhuKien.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class comment
    {
        public int id { get; set; }
        public Nullable<int> product { get; set; }
        public Nullable<int> customer { get; set; }
        public string comment1 { get; set; }
    
        public virtual customer customer1 { get; set; }
        public virtual product product1 { get; set; }
    }
}
