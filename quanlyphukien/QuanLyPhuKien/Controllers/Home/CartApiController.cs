﻿using QuanLyPhuKien.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QuanLyPhuKien.Controllers.Home
{
    public class CartApiController : ApiController
    {
        private QuanLyPhuKienEntities6 db = new QuanLyPhuKienEntities6();
        // GET: api/Cart
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Cart/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Cart
        public void Post([FromBody]int id)
        {
            product product = db.products.Find(id);
            Helper.CartController.AddToCart(product);
        }

        // PUT: api/Cart/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Cart/5
        public void Delete(int id)
        {
        }
    }
}
