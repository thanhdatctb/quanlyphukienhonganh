﻿using QuanLyPhuKien.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyPhuKien.Controllers.Home
{
    public class HomeCategoryController : Controller
    {
        QuanLyPhuKienEntities6 db = new QuanLyPhuKienEntities6();
        public ActionResult Product(int id)
        {
            List<product> products = new List<product>();
            products = db.products.Where(e => e.category == id).ToList();
            var categories = db.categories.ToList();
            dynamic mymodel = new ExpandoObject();
            mymodel.Products = products;
            mymodel.Categories = categories;
            mymodel.Category = db.categories.Find(id);
            return View(mymodel);
            
        }
        // GET: HomeCategory
        public ActionResult Index()
        {
            return View();
        }

        // GET: HomeCategory/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HomeCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HomeCategory/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HomeCategory/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HomeCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HomeCategory/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HomeCategory/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
