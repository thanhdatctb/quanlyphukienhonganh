﻿using QuanLyPhuKien.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyPhuKien.Controllers.Home
{
    public class CheckoutController : Controller
    {
        private QuanLyPhuKienEntities6 db = new QuanLyPhuKienEntities6();
        // GET: Checkout
        public ActionResult Index()
        {
            dynamic mymodel = new ExpandoObject();
            var cus = (customer)HttpContext.Session["customer"];
            if (cus == null)
            {
                return Redirect("/HomeCustomer/LogIn");
            }
            mymodel.Customer = cus;
            return View(mymodel);
        }

        // GET: Checkout/Details/5
        public ActionResult Details(int id)
        {

            return View();
        }
        
        public ActionResult Pay()
        {
            var BillId = 1;
            var cus = (customer)HttpContext.Session["customer"]; 
            try{
               BillId = db.bills.Where(e => e.customer == cus.id)
                    .Where(e => e.status == false)
                    .Max(e => e.id);
            } 
            catch(Exception ex)
            {
                bill bill = new bill();
                bill.customer = cus.id;
                bill.total = 0;
                bill.status = false;
                db.bills.Add(bill);
                db.SaveChanges();
                
            }
            BillId = db.bills.Where(e => e.customer == cus.id)
        
                    .Max(e => e.id);
            foreach (var item in MySession.Session.carts)
            {
                billDetail detail = new billDetail();
                detail.billId = BillId;
                detail.product = item.product.id;
                detail.quantity = item.quantity;
                db.billDetails.Add(detail);
                db.SaveChanges();
                bill bill = db.bills.Find(BillId);
                bill.total += item.total;
                db.Entry(bill).State = EntityState.Modified;
                db.SaveChanges();
                customer customer = db.customers.Find(cus.id);
                customer.total = customer.total + item.total;
                db.Entry(customer).State = EntityState.Modified;
                Helper.CartController.CheckOut();
                db.SaveChanges();
                bill = db.bills.Find(BillId);
                bill.status = true;
                db.Entry(bill).State = EntityState.Modified;
                db.SaveChanges();
            }    
            
            return Redirect("/");
        }
        // GET: Checkout/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Checkout/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Checkout/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Checkout/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Checkout/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Checkout/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
      
        
    }
}
