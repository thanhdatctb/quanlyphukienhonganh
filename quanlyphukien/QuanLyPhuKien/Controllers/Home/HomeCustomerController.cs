﻿using Microsoft.Ajax.Utilities;
using QuanLyPhuKien.Helper;
using QuanLyPhuKien.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyPhuKien.Controllers.Home
{
    public class HomeCustomerController : Controller
    {
        private QuanLyPhuKienEntities6 db = new QuanLyPhuKienEntities6();
        [HttpPost]
        public ActionResult LogIn([Bind(Include = "id,username,hashpass,pass,name,address,email,total")] customer customer)
        {
            customer.hashpass = PasswordController.Sha1Hash(customer.pass);
            //var author = db.customers.Where(a => a.hashpass == "abc");
            var author = db.customers.Where(a => a.hashpass == customer.hashpass)
            .Where(a => a.username == customer.username);
            var cus = (customer)author.FirstOrDefault();
            if(cus == null)
            {
                return View();
            }
            
            HttpContext.Session["customer"] = cus;
            var abc = (customer)HttpContext.Session["customer"];
            var name = abc.name;
            return Redirect("/");
                
            
        }
        public ActionResult LogIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LogOut()
        {
            HttpContext.Session["customer"] = null;
            return RedirectToAction("LogIn");
        }
        public ActionResult SignUp()
        {

            return View();
        }
        // GET: HomeCustomer
        public ActionResult Index()
        {
            return View();
        }

        // GET: HomeCustomer/Details/5
        public ActionResult Detail()
        {
            dynamic mymodel = new ExpandoObject();
            mymodel.Customer = (customer)HttpContext.Session["customer"];
            return View(mymodel);
        }

        // GET: HomeCustomer/Create
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
       
        public ActionResult Create([Bind(Include = "id,username,hashpass,pass,name,address,email,phone,total")] customer customer)
        {
            if (ModelState.IsValid)
            {
                customer.hashpass = PasswordController.Sha1Hash(customer.pass);
                customer.total = 0;
                db.customers.Add(customer);
                db.SaveChanges();
                return Redirect("/");
            }

            return View(customer);
        }
        // POST: HomeCustomer/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: HomeCustomer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HomeCustomer/Edit/5
        [HttpPost]
        public ActionResult Edit([Bind(Include = "id,username,name,address,email,phone,total")] customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                customer sescus = (customer)HttpContext.Session["customer"];
                customer.hashpass = sescus.hashpass;
                db.SaveChanges();
                HttpContext.Session["customer"] = (customer)db.customers.Where(a => a.id == customer.id).FirstOrDefault();
                return Redirect("/");
            }
            return Redirect("/");
        }

        // GET: HomeCustomer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HomeCustomer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
