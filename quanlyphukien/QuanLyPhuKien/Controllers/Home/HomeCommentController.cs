﻿using QuanLyPhuKien.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyPhuKien.Controllers.Home
{
    public class HomeCommentController : Controller
    {
        QuanLyPhuKienEntities6 db = new QuanLyPhuKienEntities6();
        // GET: HomeComment
        public ActionResult Index()
        {
            return View();
        }

        // GET: HomeComment/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HomeComment/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HomeComment/Create
        [HttpPost]
        public ActionResult Create([Bind(Include = "id,product,customer,comment1")] comment comment)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    
                    db.comments.Add(comment);
                    db.SaveChanges();
                    return Redirect("/");
                }

                return Redirect("/");
            }
            catch
            {
                return Redirect("/");
            }
        }

        // GET: HomeComment/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HomeComment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HomeComment/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HomeComment/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
