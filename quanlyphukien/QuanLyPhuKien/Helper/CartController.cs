﻿using Microsoft.Ajax.Utilities;
using QuanLyPhuKien.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyPhuKien.Helper
{
    public static class CartController
    {
        private static QuanLyPhuKienEntities6 db = new QuanLyPhuKienEntities6();

        public static bool AddToCart(product product, int quantity = 1)
        {
            
            foreach(var item in MySession.Session.carts)
            {
                if(item.product.id == product.id)
                {
                    item.quantity += quantity;
                    return true;
                }

            }
            CartItem cartItem = new CartItem(product, quantity);
            MySession.Session.carts.Add(cartItem);
            return true;
        }

        public static bool AddToCart(int id, int quantity = 1)
        {
            product product = db.products.Find(id);
            AddToCart(product,quantity);
            return true;
        }
        public static bool CheckOut()
        {
            MySession.Session.carts = new List<CartItem>();
            return true;
        }
        public static bool Remove(CartItem item)
        {
            MySession.Session.carts.Remove(item);
            return true;
        }
        public static bool Remove(int productId)
        {
            CartItem cartItem = new CartItem();
            foreach(var item in MySession.Session.carts)
            {
                if(item.product.id == productId)
                {
                    cartItem = item;
                    break;
                }    
            }    
            MySession.Session.carts.Remove(cartItem);
            return true;
        }
        public static double GetTotal()
        {
             double total = 0.0;
            if(MySession.Session.carts.Count !=0)
            {
                foreach (var item in MySession.Session.carts)
                {
                        total = total + (double)item.product.price * item.quantity;
                }
            }    
           
            return total;
        }
    }
}