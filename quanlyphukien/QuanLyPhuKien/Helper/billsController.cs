﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using QuanLyPhuKien.Models;

namespace QuanLyPhuKien.Helper
{
    public class billsController : ApiController
    {
        private QuanLyPhuKienEntities6 db = new QuanLyPhuKienEntities6();

        // GET: api/bills
        public IQueryable<bill> Getbills()
        {
            return db.bills;
        }

        // GET: api/bills/5
        [ResponseType(typeof(bill))]
        public IHttpActionResult Getbill(int id)
        {
            bill bill = db.bills.Find(id);
            if (bill == null)
            {
                return NotFound();
            }

            return Ok(bill);
        }

        // PUT: api/bills/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putbill(int id, bill bill)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bill.id)
            {
                return BadRequest();
            }

            db.Entry(bill).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!billExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/bills
        [ResponseType(typeof(bill))]
        public IHttpActionResult Postbill(bill bill)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.bills.Add(bill);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = bill.id }, bill);
        }

        // DELETE: api/bills/5
        [ResponseType(typeof(bill))]
        public IHttpActionResult Deletebill(int id)
        {
            bill bill = db.bills.Find(id);
            if (bill == null)
            {
                return NotFound();
            }

            db.bills.Remove(bill);
            db.SaveChanges();

            return Ok(bill);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool billExists(int id)
        {
            return db.bills.Count(e => e.id == id) > 0;
        }
    }
}