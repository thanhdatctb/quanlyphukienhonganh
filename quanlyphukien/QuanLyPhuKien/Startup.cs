﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QuanLyPhuKien.Startup))]
namespace QuanLyPhuKien
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
